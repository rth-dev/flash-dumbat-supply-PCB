# flash dumbat supply
voltage converter to be used with dummy batteries to supply camera flashes

![idea](https://gitlab.com/rth-dev/flash-dumbat-supply-PCB/-/raw/master/images/the_idea_of_flash-dumbat-supply.png)


* **Rev_A** is intended be used only verifying the schematic and testing the capability of the LM22680 voltage regulator
* **Rev_B** is ready for use of the LM22680 voltage regulator for powering flashes, the trimmer should be set as needed
* **Rev_C** convenience update (test pins, RT/SYNC series resistor)
 ![Rev C](https://gitlab.com/rth-dev/flash-dumbat-supply-PCB/-/raw/master/revisions/Rev_C/flash-dumbat-supply%20Rev_C%20front.png)
